#include <cstdarg>
#include <iostream>
#include <string>
using namespace std;

int err_sys(const char *err_msg) {
  cerr << err_msg << endl;
  exit(-1);
}

int err_sys(string err_msg, ...) {
  char *add_msg;
  va_list vl;

  va_start(vl, err_msg);

  while ((add_msg = va_arg(vl, char *)) != NULL) {
    cout << add_msg << endl;
    err_msg += " ";
    err_msg += add_msg;
  }

  cerr << err_msg << endl;

  va_end(vl);

  exit(-1);
}

int clean_breakline(char *line, int len) {
  for (int i = len - 1; i >= 0; i--) {
    if (!(line[i] == '\n' || line[i] == '\r')) {
      line[i + 1] = '\0';
      break;
    }
  }
  return 0;
}
