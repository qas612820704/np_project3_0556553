#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "include/define.h"
#include "include/hw3_cgi.h"
#include "include/hw4_cgi.h"
#include "include/ultis.h"

using namespace std;

int create_server(int port);
int cgi_server(int);
int normal_cgi_server(int);
string get_request_path(string);
string get_name(string);
string get_qstr(string);

int main(int argc, char const *argv[]) {
  sockaddr_in cli_addr;
  unsigned int cli_addr_len = sizeof(cli_addr);

  int listenfd, connfd;
  int pid;

  int port;
  if (argc >= 2) {
    port = atoi(argv[1]);
  } else {
    port = PORT_DEFAULT;
  }

  listenfd = create_server(port);

  while (true) {
    bzero(&cli_addr, cli_addr_len);

    if ((connfd = accept(listenfd, (sockaddr *)&cli_addr, &cli_addr_len)) < 0)
      err_sys("server: accpet error");

    if ((pid = fork()) < 0)
      err_sys("server: fork error");
    else if (pid == 0) {
      close(listenfd);

      cgi_server(connfd);

      close(connfd);

      exit(0);
    } else {
      close(connfd);
    }
  }

  return 0;
}

int create_server(int port) {
  int listen_fd;

  sockaddr_in server_addr;

  bzero(&server_addr, sizeof(server_addr));

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_addr.sin_port = htons(port);

  if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    err_sys("server: can't open stream socket");

  if (bind(listen_fd, (sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
    err_sys("server: can't bind address", to_string(port).c_str(), NULL);
  }

  listen(listen_fd, MAX_CLIENT);

  return listen_fd;
}

int cgi_server(int sockfd) {
  char line[MAX_LINE_LEN];

  int len = read(sockfd, line, MAX_LINE_LEN);

  if (len == 0) {
    exit(0);
  }

  string path = get_request_path(line);
  string file = get_name(path);
  string qstr = get_qstr(path);

  string HTTPOK = "HTTP/1.1 200 OK\r\n";
  string TYPEHTML = "Content-Type: text/html\r\n";

  if (file.empty()) {
    write(sockfd, HTTPOK.c_str(), HTTPOK.length());
    write(sockfd, TYPEHTML.c_str(), TYPEHTML.length());
    write(sockfd, "\r\n", 2);

    string f_fghtml = HTML_ROOT;
    f_fghtml += F_FORM_GET;

    ifstream ifs(f_fghtml.c_str());

    string f_line;

    while (ifs) {
      getline(ifs, f_line);
      write(sockfd, f_line.c_str(), f_line.length());
    }
  } else if (file.compare("hw4") == 0) {
    write(sockfd, HTTPOK.c_str(), HTTPOK.length());
    write(sockfd, TYPEHTML.c_str(), TYPEHTML.length());
    write(sockfd, "\r\n", 2);

    string f_fghtml2 = HTML_ROOT;
    f_fghtml2 += F_FORM_GET2;

    ifstream ifs(f_fghtml2.c_str());

    string f_line;

    while (ifs) {
      getline(ifs, f_line);
      write(sockfd, f_line.c_str(), f_line.length());
    }

  } else if (file.compare("hw3.cgi") == 0) {
    write(sockfd, HTTPOK.c_str(), HTTPOK.length());
    write(sockfd, TYPEHTML.c_str(), TYPEHTML.length());
    write(sockfd, "\r\n", 2);

    hw3_cgi_server(sockfd, qstr);

  } else if (file.compare("hw4.cgi") == 0) {

    hw4_cgi_server(sockfd, qstr);
  } else {
    string full_cgi_path = CGI_ROOT + file;

    int pid;

    if ((pid = fork()) < 0)
      err_sys("server: fork error");
    else if (pid == 0) {
      dup2(sockfd, STDIN_FILENO);
      dup2(sockfd, STDOUT_FILENO);
      dup2(sockfd, STDERR_FILENO);

      write(sockfd, HTTPOK.c_str(), HTTPOK.length());

      execl(full_cgi_path.c_str(), full_cgi_path.c_str(), NULL);

    } else {
      wait(NULL);
      close(sockfd);
    }
  }

  return 0;
}

string get_request_path(string line) {
  istringstream iss(line);
  string entry;

  // Just consider first line
  // ex: GET / HTTP/1.1
  getline(iss, entry);

  // iss set to the first line
  iss.str(entry);

  // ex: iss -> 'GET' -> '/'
  // entry = '/'
  iss >> entry >> entry;

  entry.erase(0, 1);

  return entry;
}

string get_name(string path) {
  int qstr_start = path.find_first_of('?');
  if (qstr_start) {
    return path.substr(0, qstr_start - 0);
  } else {
    return path;
  }
}

string get_qstr(string path) {
  int qstr_start = path.find_first_of('?');
  if (qstr_start) {
    return path.substr(qstr_start + 1);
  } else {
    return "";
  }
}
