#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "include/define.h"
#include "include/hw3_cgi.h"
#include "include/hw4_cgi.h"
#include "include/ultis.h"

using namespace std;

int find_max_fd4(vector<Server4> s_ary);
int handle_msg4(int sockfd, vector<Server4> s_ary);
std::vector<Server4> parse_qstr4(std::string qstr);

int hw4_cgi_server(int sockfd, string qstr) {
  vector<Server4> server_ary = parse_qstr4(qstr);

  string HTTPOK = "HTTP/1.1 200 OK\r\n";
  string TYPEHTML = "Content-Type: text/html\r\n";
  write(sockfd, HTTPOK.c_str(), HTTPOK.length());
  write(sockfd, TYPEHTML.c_str(), TYPEHTML.length());
  write(sockfd, "\r\n", 2);

  string tmp;
  tmp += HTML_ROOT;
  tmp += HW3_HTML;

  ifstream ifs(tmp);

  while (ifs) {
    tmp = "";
    getline(ifs, tmp);
    ostringstream oss;
    oss.str("");
    if (tmp[0] == '?') {
      switch (tmp[1] - '0') {
      case 1:
        for (int i = 0; i < server_ary.size(); i++) {
          oss << "<td>" << inet_ntoa(server_ary[i].serv_addr.sin_addr)
              << "</td>";
        }
        tmp = oss.str();
        break;
      case 2:
        for (int i = 0; i < server_ary.size(); i++) {
          oss << "<td valign=\"top\" id=\"m" << server_ary[i].id << "\">"
              << "</td>";
        }
        tmp = oss.str();
        break;
      case 3:
        handle_msg4(sockfd, server_ary);
        tmp = "";
        break;
      default:
        err_sys("Unknow: template");

        break;
      }
    }

    write(sockfd, tmp.c_str(), tmp.length());
  }
  return 0;
}

vector<Server4> parse_qstr4(string qstr) {

  vector<Server4> server_ary(MAX_SERVER);
  vector<int> server_isset(MAX_SERVER);
  for (int i = 0; i < server_isset.size(); i++) {
    server_isset[i] = 0;
  }

  istringstream iss_amp, iss_eq;
  string tmp_str;

  iss_amp.str(qstr);
  while (iss_amp) {
    tmp_str.erase();
    getline(iss_amp, tmp_str, '&');

    string key;
    string value;
    int eq_pos = tmp_str.find('=');
    if (eq_pos) {
      key = tmp_str.substr(0, eq_pos);
      value = tmp_str.substr(eq_pos + 1);
      if (value.empty()) {
        continue;
      }
    } else {
      continue;
    }

    int id;
    if (key[0] == 's') {
      id = key[2] - '0';
    } else {
      id = key[1] - '0';
    }
    int ary_pos = id - 1;

    server_ary[ary_pos].serv_addr.sin_family = AF_INET;
    server_ary[ary_pos].proxy_addr.sin_family = AF_INET;
    server_ary[ary_pos].id = id;

    if (key[0] == 'h') {
      hostent *he = gethostbyname(value.c_str());
      memcpy(&server_ary[ary_pos].serv_addr.sin_addr, he->h_addr, he->h_length);
    } else if (key[0] == 'p') {
      server_ary[ary_pos].serv_addr.sin_port = htons(atoi(value.c_str()));
    } else if (key[0] == 'f') {

      tmp_str = BATCH_ROOT + value;

      ifstream ifs(tmp_str);

      while (ifs) {
        tmp_str = "";
        getline(ifs, tmp_str);
        if (!tmp_str.empty()) {
          server_ary[ary_pos].file_content.push_back(tmp_str);
        }
      }
    } else if (key[0] == 's' && key[1] == 'h') {
      hostent *he = gethostbyname(value.c_str());
      memcpy(&server_ary[ary_pos].proxy_addr.sin_addr, he->h_addr,
             he->h_length);
    } else if (key[0] == 's' && key[1] == 'p') {
      server_ary[ary_pos].proxy_addr.sin_port = htons(atoi(value.c_str()));
    } else {
      cerr << "Unknow KEY:" << key << endl;
      exit(-1);
    }
    server_isset[ary_pos]++;
  }

  for (int i = 0; i < server_ary.size(); i++) {
    if (server_isset[i] != 5) {
      server_ary.erase(server_ary.begin() + i);
      i--;
      continue;
    }
    server_ary[i].n_content = 0;
    server_ary[i].n_writed = 0;
  }

  return server_ary;
}

int handle_msg4(int sockfd, vector<Server4> s_ary) {

  for (int i = 0; i < s_ary.size(); i++) {
    if ((s_ary[i].sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
      err_sys("server: can't open stream socket");

    int flags = fcntl(s_ary[i].sockfd, F_GETFL, 0);
    fcntl(s_ary[i].sockfd, F_SETFL, flags | O_NONBLOCK);

    if (connect(s_ary[i].sockfd, (sockaddr *)&s_ary[i].proxy_addr,
                sizeof(s_ary[i].proxy_addr)) < 0)
      if (errno != EINPROGRESS)
        err_sys("server: can't connect",
                inet_ntoa(s_ary[i].proxy_addr.sin_addr),
                to_string(ntohs(s_ary[i].proxy_addr.sin_port)).c_str(), NULL);

    s_ary[i].state = S_WRITING_SOCKV4;
  }

  fd_set rfds, wfds;
  fd_set rs, ws;

  int nfds = find_max_fd4(s_ary) + 1;
  int n_conn = s_ary.size();

  FD_ZERO(&rfds);
  FD_ZERO(&wfds);
  FD_ZERO(&rs);
  FD_ZERO(&ws);

  for (int i = 0; i < s_ary.size(); i++) {
    FD_SET(s_ary[i].sockfd, &rs);
    FD_SET(s_ary[i].sockfd, &ws);
  }

  rfds = rs;
  wfds = ws;

  while (n_conn > 0) {

    memcpy(&rfds, &rs, sizeof(rfds));
    memcpy(&wfds, &ws, sizeof(wfds));

    if (select(nfds, &rfds, &wfds, (fd_set *)0, (timeval *)0) < 0)
      err_sys("server: select error");

    for (int i = 0; i < s_ary.size(); i++) {
      Server4 *server = &s_ary[i];

      int error = 0;
      unsigned int error_n = 0;

      int len = 0;
      string str_towrite = "";
      char line[MAX_LINE_LEN];

      if (server->state == S_WRITING && FD_ISSET(server->sockfd, &wfds)) {
        str_towrite =
            server->file_content[server->n_content].substr(server->n_writed) +
            "\r\n";
        len = write(server->sockfd, str_towrite.c_str(), str_towrite.length());
        server->n_writed += len;
        if (len <= 0 ||
            server->n_writed >=
                server->file_content[server->n_content].length() - 1) {

          ostringstream oss("");
          string str_tmp;
          oss << "<script>document.all['m" << server->id
              << "'].innerHTML += \"<b>"
              << str_encode(server->file_content[server->n_content])
              << "</b><br>\";</script>";
          str_tmp = oss.str();
          write(sockfd, str_tmp.c_str(), str_tmp.length());

          server->n_content++;
          server->n_writed = 0;
          server->state = S_READING;

          FD_CLR(server->sockfd, &ws);
          FD_SET(server->sockfd, &rs);
        }
      } else if (server->state == S_READING &&
                 FD_ISSET(server->sockfd, &rfds)) {
        bzero(line, MAX_LINE_LEN);
        len = read(server->sockfd, line, MAX_LINE_LEN);

        string str_line = str_encode(line);

        if (len <= 0) {

          server->state = S_DONE;
          n_conn--;

          FD_CLR(server->sockfd, &rs);
        } else {
          string str_tmp;
          ostringstream oss("");
          oss << "<script>document.all['m" << server->id << "'].innerHTML += \""
              << str_line << "\";</script>";
          str_tmp = oss.str();
          write(sockfd, str_tmp.c_str(), str_tmp.length());
          if (str_line.find("% ") != string::npos) {
            FD_SET(server->sockfd, &ws);
            FD_CLR(server->sockfd, &rs);
            server->state = S_WRITING;
          }
        }
      } else if (server->state == S_WRITING_SOCKV4 &&
                 FD_ISSET(server->sockfd, &wfds)) {
        unsigned char sockv4_req_pkg[8];
        sockv4_req_pkg[0] = 0x04;
        sockv4_req_pkg[1] = 0x01;
        sockv4_req_pkg[2] =
            (unsigned char)(ntohs(server->serv_addr.sin_port) / 256);
        sockv4_req_pkg[3] =
            (unsigned char)(ntohs(server->serv_addr.sin_port) % 256);
        sockv4_req_pkg[4] =
            (unsigned char)(server->serv_addr.sin_addr.s_addr % 256);
        sockv4_req_pkg[5] =
            (unsigned char)((server->serv_addr.sin_addr.s_addr >> 8) % 256);
        sockv4_req_pkg[6] =
            (unsigned char)((server->serv_addr.sin_addr.s_addr >> 16) % 256);
        sockv4_req_pkg[7] =
            (unsigned char)((server->serv_addr.sin_addr.s_addr >> 24) % 256);

        len = write(server->sockfd, sockv4_req_pkg, 8);
        if (len < 8)
          err_sys("Write sockv4_req_pkg < 8");

        FD_CLR(server->sockfd, &ws);
        FD_SET(server->sockfd, &rs);
        server->state = S_READING_SOCKV4;
      } else if (server->state == S_READING_SOCKV4 &&
                 FD_ISSET(server->sockfd, &rfds)) {
        unsigned char sockv4_res_pkg[8];
        bzero(sockv4_res_pkg, 8);
        len = read(s_ary[i].sockfd, sockv4_res_pkg, 8);
        if (len < 8)
          err_sys("Read sockv4_res_pkg < 8");

        unsigned char CD = sockv4_res_pkg[1];
        if (CD != 0x5A)
          err_sys("CD != 0x5A");

        FD_CLR(server->sockfd, &ws);
        FD_SET(server->sockfd, &rs);
        server->state = S_READING;
      }
    }
  }

  return 0;
}

int find_max_fd4(vector<Server4> s_ary) {
  int max_fd = -1;
  for (int i = 0; i < s_ary.size(); i++) {
    if (s_ary[i].sockfd > max_fd) {
      max_fd = s_ary[i].sockfd;
    }
  }
  if (max_fd < 0) {
    err_sys("find_max_fd: max_fd < 0");
  }
  return max_fd;
}
