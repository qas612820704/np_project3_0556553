#define MAX_CLIENT 30

#define PORT_DEFAULT 8000

#define CGI_ROOT "/home/lego/workspace/networkProgramming/hw3/cgi/"

#define BATCH_ROOT "/home/lego/workspace/networkProgramming/hw3/batch/"

#define HTML_ROOT "/home/lego/workspace/networkProgramming/hw3/html/"

#define F_FORM_GET "form_get.html"
#define F_FORM_GET2 "form_get2.html"

#define HW3_HTML "hw3_cgi.html"

#define HOME "/home/lego/workspace/networkProgramming/hw3/rwg_server/home/rwg/"

#define MAX_LINE_LEN 15000

#define MAX_SERVER 5
