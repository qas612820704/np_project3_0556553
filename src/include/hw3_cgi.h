#include <string>
#include <vector>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>

#define S_CONNECTING 0
#define S_READING 1
#define S_WRITING 2
#define S_DONE 3

class Server {
public:
  int id;
  sockaddr_in serv_addr;

  std::vector<std::string> file_content;

  int n_content;
  int n_writed;

  int sockfd;

  int state;
};

int find_max_fd(std::vector<Server> s_ary);
std::string str_repace(std::string str, std::string pattern, std::string to);
std::string str_encode(std::string str);

int hw3_cgi_server(int, std::string);
