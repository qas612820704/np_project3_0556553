#include <string>
#include <vector>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/wait.h>

#define S_READING_SOCKV4 4
#define S_WRITING_SOCKV4 5

int hw4_cgi_server(int, std::string);

class Server4 {
public:
  int id;
  sockaddr_in serv_addr;
  sockaddr_in proxy_addr;

  std::vector<std::string> file_content;

  int n_content;
  int n_writed;

  int sockfd;

  int state;
};
